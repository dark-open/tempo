<?php

require dirname(__DIR__).'/tests/bootstrap.php';
$kernel = new Tempo\Kernel('test', true);
$kernel->boot();

return $kernel->getContainer()->get('doctrine')->getManager();
