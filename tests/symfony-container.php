<?php

require __DIR__.'/../vendor/autoload.php';

$appKernel = new Tempo\Kernel('tests', false);
$appKernel->boot();

return $appKernel->getContainer();
