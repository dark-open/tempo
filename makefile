.PHONY: format test_format help
.DEFAULT_GOAL= help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
##
##
## RESPECT DES NORMES PSR-2 / Symfony
##
##
format: ## Format les fichiers à la norme PSR-2 / Symfony
	vendor/bin/php-cs-fixer fix --verbose

analyse: ## Exécute phpstan
	vendor/bin/phpstan analyse --memory-limit 4G src --level 6