<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250130143000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Nouveaux tarifs au 1er fevrier 2025';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO prices (start_at,blue_low_price, blue_high_price, white_low_price, white_high_price, red_low_price, red_high_price) VALUES ('2025-02-01 00:00:00', 1288, 1552, 1447, 1792, 1518, 6586);");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM prices WHERE start_at = '2025-02-01 00:00:00';");
    }
}
