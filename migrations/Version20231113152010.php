<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231113152010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tempo_web_push_subscription (id INT AUTO_INCREMENT NOT NULL, end_point VARCHAR(255) NOT NULL, public_key VARCHAR(255) NOT NULL, auth_token VARCHAR(255) NOT NULL, content_encoding VARCHAR(255) DEFAULT NULL, is_blue TINYINT(1) NOT NULL, is_white TINYINT(1) NOT NULL, is_red TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tempo_users_stats ADD web_push TIME DEFAULT NULL COMMENT \'(DC2Type:time_immutable)\'');
        $this->addSql('ALTER TABLE tempo_users_stats ADD push_subscriptions INT NOT NULL, ADD push_unsubscribes INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE IF EXISTS tempo_web_push_subscription');
        $this->addSql('ALTER TABLE tempo_users_stats DROP web_push');
        $this->addSql('ALTER TABLE tempo_users_stats DROP push_subscriptions, DROP push_unsubscribes');
    }
}
