<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241114145729 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tempo_color_day ADD prices_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tempo_color_day ADD CONSTRAINT FK_66271132D9C9DE39 FOREIGN KEY (prices_id) REFERENCES prices (id)');
        $this->addSql('CREATE INDEX IDX_66271132D9C9DE39 ON tempo_color_day (prices_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tempo_color_day DROP FOREIGN KEY FK_66271132D9C9DE39');
        $this->addSql('DROP INDEX IDX_66271132D9C9DE39 ON tempo_color_day');
        $this->addSql('ALTER TABLE tempo_color_day DROP prices_id');
    }
}
