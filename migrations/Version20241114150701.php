<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241114150701 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Insertion des tarifs depuis 2020';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO prices (start_at,blue_low_price, blue_high_price, white_low_price, white_high_price, red_low_price, red_high_price) VALUES ('2020-02-01 00:00:00', 1216, 1462, 1289, 1620, 1372, 6184);");
        $this->addSql("INSERT INTO prices (start_at,blue_low_price,blue_high_price,white_low_price,white_high_price,red_low_price,red_high_price) VALUES ( '2020-08-01 00:00:00',1246, 1548, 1397, 1757, 1491, 6389);");
        $this->addSql("INSERT INTO prices (start_at,blue_low_price,blue_high_price,white_low_price,white_high_price,red_low_price,red_high_price) VALUES ('2021-02-01 00:00:00',1246,1548,1397,1757,1491,6389);");
        $this->addSql("INSERT INTO prices (start_at,blue_low_price,blue_high_price,white_low_price,white_high_price,red_low_price,red_high_price) VALUES ('2023-02-01 00:00:00',970,1249,1140,1508,1216,6712);");
        $this->addSql("INSERT INTO prices (start_at,blue_low_price,blue_high_price,white_low_price,white_high_price,red_low_price,red_high_price) VALUES ('2023-08-01 00:00:00',1328,1654,1486,1894,1328,7324);");
        $this->addSql("INSERT INTO prices (start_at,blue_low_price,blue_high_price,white_low_price,white_high_price,red_low_price,red_high_price) VALUES ('2024-02-01 00:00:00',1296,1609,1486,1894,1568,7562);");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM prices WHERE start_at = '2020-02-01 00:00:00';");
        $this->addSql("DELETE FROM prices WHERE start_at = '2020-08-01 00:00:00';");
        $this->addSql("DELETE FROM prices WHERE start_at = '2021-02-01 00:00:00';");
        $this->addSql("DELETE FROM prices WHERE start_at = '2023-02-01 00:00:00';");
        $this->addSql("DELETE FROM prices WHERE start_at = '2023-08-01 00:00:00';");
        $this->addSql("DELETE FROM prices WHERE start_at = '2024-02-01 00:00:00';");
    }
}
