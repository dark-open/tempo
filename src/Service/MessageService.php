<?php

namespace Tempo\Service;

use Tempo\Entity\Message;
use Tempo\Repository\MessageRepository;
use Tempo\Tools\ToolsDate;

class MessageService
{
    public function __construct(
        private readonly MessageRepository $messageRepository,
    ) {
    }

    /** @return Message[] */
    public function getMessages(?\DateTime $date = null, bool $returnJson = false): array
    {
        $date = $date ?? ToolsDate::getNow();
        $messages = $this->messageRepository->getAllMessagesToDisplayToday($date);

        if ($returnJson) {
            $messagesJson = [];
            foreach ($messages as $message) {
                $messagesJson[] = $message->getMessage();
            }
            $messages = $messagesJson;
        }

        return $messages;
    }
}
