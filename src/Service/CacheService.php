<?php

namespace Tempo\Service;

use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Tempo\Tools\ToolsDate;

class CacheService
{
    public const PREFIX_DATA = 'color_day';
    public const PREFIX_OLDEST = self::PREFIX_DATA.'_oldest';
    public const PREFIX_STATS_WEB = 'stats_web';
    public const PREFIX_STATS_API = 'stats_web';
    public const PREFIX_STATS_RSS = 'stats_rss';
    public const PREFIX_STATS_CALENDAR = 'stats_calendar';
    public const PREFIX_STATS_WEB_PUSH = 'stats_webPush';
    public const PREFIC_STATS_YEAR = 'stats_year';

    public function __construct(
        private readonly CacheInterface $tempoCache,
        private readonly LoggerInterface $prodLogger)
    {
    }

    public function getColorDayId(?\DateTime $today = null): string
    {
        return $this->getIdByPrefix(self::PREFIX_DATA, $today);
    }

    public function getOldestColorDayId(): string
    {
        return self::PREFIX_OLDEST;
    }

    public function getStatWebId(?\DateTime $today = null): string
    {
        return $this->getIdByPrefix(self::PREFIX_STATS_WEB, $today);
    }

    public function getStatApiId(?\DateTime $today = null): string
    {
        return $this->getIdByPrefix(self::PREFIX_STATS_API, $today);
    }

    public function getStatRssId(?\DateTime $today = null): string
    {
        return $this->getIdByPrefix(self::PREFIX_STATS_RSS, $today);
    }

    public function getStatCalendarId(?\DateTime $today = null): string
    {
        return $this->getIdByPrefix(self::PREFIX_STATS_CALENDAR, $today);
    }

    public function getStatWebPushId(?\DateTime $today = null): string
    {
        return $this->getIdByPrefix(self::PREFIX_STATS_WEB_PUSH, $today);
    }

    public function getStatYearId(\DateTime $today): string
    {
        return $this->getIdByPrefix(self::PREFIC_STATS_YEAR, $today);
    }

    public function get(string $cacheId, callable $function): mixed
    {
        $data = null;
        try {
            $data = $this->tempoCache->get($cacheId, $function);
        } catch (InvalidArgumentException $e) {
            $this->prodLogger->error($e);
        }

        return $data;
    }

    public function deleteColorDayCache(?\DateTime $date = null): void
    {
        $date = $date ?? ToolsDate::getNow();
        $idCache = $this->getColorDayId($date);

        try {
            $this->tempoCache->delete($idCache);
        } catch (InvalidArgumentException $e) {
            $this->prodLogger->error($e);
        }
    }

    protected function getIdByPrefix(string $prefix, ?\DateTime $today = null): string
    {
        $today = $today ?? ToolsDate::getNow();

        return $prefix.'-'.$today->format('Ymd');
    }
}
