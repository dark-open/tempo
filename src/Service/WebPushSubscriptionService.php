<?php

namespace Tempo\Service;

use Minishlink\WebPush\Subscription;
use Minishlink\WebPush\WebPush;
use Tempo\Entity\WebPushSubscription;
use Tempo\Enum\Color;
use Tempo\Repository\WebPushSubscriptionRepository;
use Tempo\Tools\ToolsDate;

class WebPushSubscriptionService
{
    protected WebPushSubscriptionRepository $webPushSubscriptionRepository;
    protected ColorDayService $colorDayService;
    protected UsersStatsService $usersStatsService;
    protected WebPush $webPush;

    /**
     * @throws \ErrorException
     */
    public function __construct(
        string $publicKey,
        string $privateKey,
        WebPushSubscriptionRepository $webPushSubscriptionRepository,
        ColorDayService $colorDayService,
        UsersStatsService $usersStatsService,
    ) {
        $this->webPushSubscriptionRepository = $webPushSubscriptionRepository;
        $this->colorDayService = $colorDayService;
        $this->usersStatsService = $usersStatsService;
        $this->webPush = new WebPush([
            'VAPID' => [
                'subject' => 'https://tempo.lhoir.me/',
                'publicKey' => $publicKey,
                'privateKey' => $privateKey,
            ],
        ]);
    }

    public function addNewSubscription(WebPushSubscription $webPushSubscription): WebPushSubscription
    {
        $subscriptionInDatabase = $this->webPushSubscriptionRepository->findOneBy([
            'endPoint' => $webPushSubscription->getEndPoint(),
        ]);

        if (!is_null($subscriptionInDatabase)) {
            $subscriptionInDatabase
                ->setIsBlue($webPushSubscription->isIsBlue())
                ->setIsWhite($webPushSubscription->isIsWhite())
                ->setIsRed($webPushSubscription->isIsRed());
        } else {
            $subscriptionInDatabase = $webPushSubscription;
        }

        $this->webPushSubscriptionRepository->save($subscriptionInDatabase, true);

        return $subscriptionInDatabase;
    }

    /**
     * @return array<int,string>
     *
     * @throws \ErrorException
     * @throws \Exception
     */
    public function sendNotificationOfTheDay(): array
    {
        $reports = [];
        $tomorrow = ToolsDate::getNextDay();
        $colorOfTomorrow = $this->colorDayService->getColorDay($tomorrow);

        if (Color::Undefined === $colorOfTomorrow->getColor()) {
            throw new \Exception('La couleur de demain n\'est pas encore définie');
        }

        $notificationMessage = $this->colorDayService->getNotificationMessageOfTheColorDay($colorOfTomorrow);

        $defaultOptions = [
            'TTL' => 86400,
            'urgency' => 'normal',
            'batchSize' => 200,
        ];
        $this->webPush->setDefaultOptions($defaultOptions);

        $subscriptions = $this->webPushSubscriptionRepository->getSubscriptionByColor($colorOfTomorrow->getColor());
        foreach ($subscriptions as $subscription) {
            $this->putMessageInQueue($subscription, $notificationMessage->getTitle(), $notificationMessage->getMessage());
        }

        $reports = $this->flushAndGetReports();

        // statistiques
        $total = $this->webPushSubscriptionRepository->count([]);
        $unsubscribes = count($reports);
        $this->usersStatsService->saveWebPushNotificationStats($total, $unsubscribes);

        return $reports;
    }

    /**
     * @throws \ErrorException
     */
    public function sendWelcomeNotification(WebPushSubscription $webPushSubscription): void
    {
        $this->putMessageInQueue($webPushSubscription, 'Notifications activées', 'Vous recevrez chaque jours vers 10h30 les notifications pour les couleurs de jours que vous avez choisi.');

        $this->flushAndGetReports();
    }

    /**
     * @throws \ErrorException
     */
    protected function putMessageInQueue(WebPushSubscription $subscription, string $title, string $message): void
    {
        $this->webPush->queueNotification(
            Subscription::create([
                'endpoint' => $subscription->getEndpoint(),
                'publicKey' => $subscription->getPublicKey(),
                'authToken' => $subscription->getAuthToken(),
            ]),
            json_encode([
                'message' => $message,
                'title' => $title,
            ])
        );
    }

    /**
     * @return array<int,string>
     *
     * @throws \ErrorException
     */
    public function flushAndGetReports(): array
    {
        $reports = [];
        foreach ($this->webPush->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();

            if (!$report->isSuccess()) {
                $reports[] = $endpoint;
            }
        }

        $this->deleteUnsubscribedPeople($reports);

        return $reports;
    }

    /**
     * @param array<int,string> $reports
     */
    protected function deleteUnsubscribedPeople(array $reports): void
    {
        $this->webPushSubscriptionRepository->removeUnsubscribedPeople($reports);
    }
}
