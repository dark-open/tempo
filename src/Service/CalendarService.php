<?php

namespace Tempo\Service;

use Psr\Log\LoggerInterface;
use Tempo\Entity\ColorDay;
use Tempo\Tools\ToolsColor;
use Tempo\Tools\ToolsDate;

class CalendarService
{
    public function __construct(
        protected readonly string $icsProdId,
        protected readonly string $icsUIDSuffix,
        protected readonly string $pathToCalendarEvents,
        protected readonly string $pathToIcs,
        protected readonly ColorDayService $colorDayService,
        protected readonly LoggerInterface $prodLogger,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function addNewEventInCalendar(ColorDay $colorDay, bool $initCalendar = false, string $pathFromCommand = ''): void
    {
        if ($initCalendar) {
            $doInit = $this->initCalendar($pathFromCommand);

            if ($doInit) {
                return;
            }
        }

        if ('U' === $colorDay->getColor()->value) {
            throw new \Exception('La couleur du jour '.$colorDay->getDay()->format('d-m-Y').' n\'est pas encore définie.');
        }

        // Formatage de la date pour l'ICS
        $start_date = $colorDay->getDay()->format('Ymd');
        $end_date = $colorDay->getDay()->format('Ymd');

        $color = ToolsColor::getFrenchTextFromColor($colorDay->getColor());
        $colorEmoji = ToolsColor::getEmojiFromColor($colorDay->getColor());
        $frenchDate = ToolsDate::frenchDate($colorDay->getDay());

        $now = ToolsDate::getNow(false);
        $dtStamp = $now->format('Ymd\THis\Z');
        $uid = $dtStamp.'-'.$colorDay->getDay()->format('Ymd').$colorDay->getColor()->value.'@'.$this->icsUIDSuffix;
        $message = $this->colorDayService->getNotificationMessageOfTheColorDay($colorDay)->getMessage();
        $newEvent = "BEGIN:VEVENT\r\nUID:$uid\r\nDTSTAMP:$dtStamp\r\nSUMMARY: $colorEmoji Tempo jour $color\r\nDESCRIPTION: $message \r\nDTSTART;VALUE=DATE:$start_date\r\nDTEND;VALUE=DATE:$end_date\r\nBEGIN:VALARM\r\nACTION:DISPLAY\r\nDESCRIPTION:Le $frenchDate est un jour $color.\r\nTRIGGER:-PT4H\r\nEND:VALARM\r\nEND:VEVENT\r\n";

        if (!file_put_contents($pathFromCommand.$this->pathToCalendarEvents, $newEvent, FILE_APPEND)) {
            throw new \Exception('Impossible d\'enregistrer dans le fichier '.$this->pathToCalendarEvents);
        }
    }

    public function publishCalendar(string $pathFromCommand = ''): string
    {
        $allEvents = file_get_contents($pathFromCommand.$this->pathToCalendarEvents);

        $icalContent = $allEvents."END:VCALENDAR\r\n";

        file_put_contents($pathFromCommand.$this->pathToIcs, $icalContent);

        return $icalContent;
    }

    protected function initCalendar(string $pathFromCommand): bool
    {
        $doInit = false;
        if (!file_exists($pathFromCommand.$this->pathToIcs)) {
            file_put_contents($pathFromCommand.$this->pathToIcs, '');
        }

        if (!file_exists($pathFromCommand.$this->pathToCalendarEvents)) {
            $begin = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//$this->icsProdId//CalDAV Client//EN\r\nCALSCALE:GREGORIAN\r\n";
            $timezone = "BEGIN:VTIMEZONE\r\nTZID:Europe/Paris\r\nTZURL:https://www.tzurl.org/zoneinfo-outlook/Europe/Paris\r\nX-LIC-LOCATION:Europe/Paris\r\nBEGIN:DAYLIGHT\r\nTZNAME:CEST\r\nTZOFFSETFROM:+0100\r\nTZOFFSETTO:+0200\r\nDTSTART:19700329T020000\r\nRRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\r\nEND:DAYLIGHT\r\nBEGIN:STANDARD\r\nTZNAME:CET\r\nTZOFFSETFROM:+0200\r\nTZOFFSETTO:+0100\r\nDTSTART:19701025T030000\r\nRRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\r\nEND:STANDARD\r\nEND:VTIMEZONE\r\n";
            file_put_contents($pathFromCommand.$this->pathToCalendarEvents, $begin.$timezone);
            $allColorDays = $this->colorDayService->getAllColorDay();

            foreach ($allColorDays as $colorDay) {
                try {
                    $this->addNewEventInCalendar($colorDay, false, $pathFromCommand);
                } catch (\Exception $e) {
                    $this->prodLogger->error($e);
                }
            }
            $doInit = true;
            $this->publishCalendar($pathFromCommand);
        }

        return $doInit;
    }
}
