<?php

namespace Tempo\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Tempo\Exception\ApiException;

class RTEApiService
{
    protected HttpClientInterface $httpClient;

    protected string $rteIdClient;
    protected string $rteIdSecret;
    protected string $rteUrlToken;
    protected string $rteUrlCalendar;

    public function __construct(
        string $rteIdClient,
        string $rteIdSecret,
        string $rteUrlToken,
        string $rteUrlCalendar,
        private readonly LoggerInterface $prodLogger,
    ) {
        $this->rteIdClient = $rteIdClient;
        $this->rteIdSecret = $rteIdSecret;
        $this->rteUrlToken = $rteUrlToken;
        $this->rteUrlCalendar = $rteUrlCalendar;

        $this->httpClient = HttpClient::create();
    }

    /**
     * @return array<string, mixed>
     *
     * @throws ApiException
     */
    public function getColorByDate(?\DateTime $today = null): array
    {
        if (is_null($today)) {
            $today = new \DateTime();
        }

        try {
            $today->setTime(0, 0);
            $interval = new \DateInterval('P1D');
            $tomorrow = (clone $today)->add($interval);
            $responseCalendars = $this->getTempoLikeCalendar($today, $tomorrow);
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            $this->prodLogger->error($e);
            throw new ApiException($e->getMessage(), $e->getCode());
        }

        return [
            'date' => $today,
            'color' => $responseCalendars['tempo_like_calendars']['values'][0]['value'],
        ];
    }

    /**
     * @return array<int,array<string,\DateTime|string>>
     *
     * @throws ApiException
     */
    public function getColorByDates(\DateTime $firstDay, \DateTime $lastDay): array
    {
        $array = [];

        $tempDay = clone $firstDay;
        $interval = new \DateInterval('P1D');
        while ($tempDay <= $lastDay) {
            $array[$tempDay->getTimestamp()] = ['day' => $tempDay, 'color' => 'Undefined'];
            $tempDay = (clone $tempDay)->add($interval);
        }

        $lastDay = $lastDay->add($interval);
        try {
            $responseCalendars = $this->getTempoLikeCalendar($firstDay, $lastDay);
            $key = 'start_date';
            $arrayRTE = $responseCalendars['tempo_like_calendars']['values'];
            usort($arrayRTE, function ($a, $b) use ($key) {
                return strtotime($a[$key]) - strtotime($b[$key]);
            });
            foreach ($arrayRTE as $line) {
                $date = \DateTime::createFromFormat('Y-m-d\TH:i:s', substr($line['start_date'], 0, 19));

                if (isset($array[$date->getTimestamp()])) {
                    $array[$date->getTimestamp()]['color'] = $line['value'];
                }
            }
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            $this->prodLogger->error($e);
            throw new ApiException($e->getMessage(), $e->getCode());
        }

        return $array;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws ApiException
     */
    protected function getToken(string $idClient, string $idSecret): string
    {
        $options = [
            'headers' => [
                'Authorization' => 'Basic '.base64_encode($idClient.':'.$idSecret),
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'body' => http_build_query([]),
        ];

        $response = $this->httpClient->request(Request::METHOD_POST, $this->rteUrlToken, $options);

        if (200 !== $response->getStatusCode()) {
            $message = 'Impossible d\'obtenir un token depuis l\'API RTE : '.$response->getStatusCode().' : '.$response->getContent();
            $this->prodLogger->error($message);
            throw new ApiException($message);
        }

        return $response->toArray()['access_token'];
    }

    /**
     * @return array<string,mixed>
     *
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws ApiException
     */
    protected function getTempoLikeCalendar(
        \DateTime $startDate,
        \DateTime $endDate,
        bool $fallbackStatus = true,
    ): array {
        $response = $this->httpClient->request(Request::METHOD_GET, $this->rteUrlCalendar, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$this->getToken($this->rteIdClient, $this->rteIdSecret),
            ],
            'query' => [
                'start_date' => $startDate->format('Y-m-d\TH:i:sP'),
                'end_date' => $endDate->format('Y-m-d\TH:i:sP'),
                'fallback_status' => $fallbackStatus ? 'true' : 'false',
            ],
        ]);

        return $response->toArray();
    }
}
