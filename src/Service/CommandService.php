<?php

namespace Tempo\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Tempo\Entity\ColorDay;
use Tempo\Enum\Color;
use Tempo\Exception\ApiException;
use Tempo\Object\CommandReport;
use Tempo\Tools\ToolsDate;

class CommandService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly CalendarService $calendarService,
        private readonly ColorDayService $colorDayService,
        private readonly WebPushSubscriptionService $webPushSubscriptionService,
        private readonly PricesService $pricesService,
        private readonly RTEApiService $RTEApiService,
        private readonly RssService $rssService,
        private readonly LoggerInterface $prodLogger,
    ) {
    }

    public function setColorOfTheDayByDate(\DateTime $dateTime): CommandReport
    {
        $commandReport = new CommandReport(true, '');
        try {
            $todayColorString = $this->RTEApiService->getColorByDate($dateTime)['color'];
            if (!$todayColorString) {
                throw new \Exception('L\'API ne retourne pas de résultat');
            }
            $colorDay = $this->colorDayService->setOrUpdateColorOfTheDay($dateTime, Color::getColorFromString($todayColorString), true);
            $previousDay = ToolsDate::getPreviousDay($dateTime);
            $previousColorDay = $this->colorDayService->getColorDay($previousDay, true);

            $colorDay
                ->setBlueLeft((Color::Blue === $colorDay->getColor()) ? $previousColorDay->getBlueLeft() - 1 : $previousColorDay->getBlueLeft())
                ->setWhiteLeft((Color::White === $colorDay->getColor()) ? $previousColorDay->getWhiteLeft() - 1 : $previousColorDay->getWhiteLeft())
                ->setRedLeft((Color::Red === $colorDay->getColor()) ? $previousColorDay->getRedLeft() - 1 : $previousColorDay->getRedLeft());
            $this->em->flush();
            $this->colorDayService->deleteColorDayCache($dateTime);

            return $commandReport->setMessage('Color of the day '.$dateTime->format('Y/m/d').' is set to '.$colorDay->getColor()->value);
        } catch (ApiException|\Exception $e) {
            $this->prodLogger->warning($e);

            try {
                if (ToolsDate::isSunday($dateTime)) {
                    $colorDay = $this->colorDayService->setOrUpdateColorOfTheDay($dateTime, Color::getColorFromString('Blue'), true);
                    $this->colorDayService->deleteColorDayCache($dateTime);
                    $previousDay = ToolsDate::getPreviousDay($dateTime);
                    $previousColorDay = $this->colorDayService->getColorDay($previousDay, true);

                    $colorDay
                        ->setBlueLeft((Color::Blue === $colorDay->getColor()) ? $previousColorDay->getBlueLeft() - 1 : $previousColorDay->getBlueLeft())
                        ->setWhiteLeft((Color::White === $colorDay->getColor()) ? $previousColorDay->getWhiteLeft() - 1 : $previousColorDay->getWhiteLeft())
                        ->setRedLeft((Color::Red === $colorDay->getColor()) ? $previousColorDay->getRedLeft() - 1 : $previousColorDay->getRedLeft());
                    $this->em->flush();

                    $this->colorDayService->deleteColorDayCache($dateTime);

                    return $commandReport->setMessage('Color of the day '.$dateTime->format('Y/m/d').' is set to '.$colorDay->getColor()->value);
                } else {
                    throw new \Exception('Impossible d\'obtenir la couleur du jour : '.$dateTime->format('d-m-Y'));
                }
            } catch (\Exception $e) {
                $this->prodLogger->error($e);

                $commandReport
                    ->setMessage($e->getMessage())
                    ->setIsSuccess(false);

                return $commandReport;
            }
        }
    }

    public function sendNotificationOfTheDay(): CommandReport
    {
        $commandReport = new CommandReport(true, '');
        try {
            $reports = $this->webPushSubscriptionService->sendNotificationOfTheDay();
            $commandReport
                ->setMessage('Notifications envoyées avec succès, '.count($reports).' desinscription(s)');

            return $commandReport;
        } catch (\Exception $e) {
            $this->prodLogger->error($e);

            return $commandReport
                ->setIsSuccess(false)
                ->setMessage($e->getMessage());
        }
    }

    public function addNewEventInCalendar(string $pathFromCommand = ''): CommandReport
    {
        $commandReport = new CommandReport(true, '');

        try {
            $colorDay = $this->colorDayService->getColorDay(ToolsDate::getNextDay());
            $this->calendarService->addNewEventInCalendar($colorDay, true, $pathFromCommand);
            $this->calendarService->publishCalendar($pathFromCommand);

            return $commandReport->setMessage('Calendrier publié');
        } catch (\Exception $e) {
            $this->prodLogger->error($e);

            return $commandReport
                ->setIsSuccess(false)
                ->setMessage($e->getMessage());
        }
    }

    public function generateRss(): void
    {
        $this->rssService->generateRss();
    }

    public function updateAllColorDayPrices(): CommandReport
    {
        $commandReport = new CommandReport(true, '');

        try {
            $colorDays = $this->colorDayService->getAllColorDayOrdered();

            foreach ($colorDays as $colorDay) {
                if (null === $colorDay->getPrices()) {
                    $this->pricesService->setPriceToColorDay($colorDay);
                }
            }

            $this->em->flush();

            return $commandReport
                ->setMessage('Chaque jour en base a été affecté à un tarif');
        } catch (\Exception $e) {
            return $commandReport
                ->setIsSuccess(false)
                ->setMessage($e->getMessage());
        }
    }

    protected function setColorsLeft(\DateTime $dateTime, ColorDay $colorDay): void
    {
        if (ToolsDate::isFirstOfSeptember($dateTime)) {
        }
    }
}
