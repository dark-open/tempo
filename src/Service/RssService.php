<?php

namespace Tempo\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Tempo\Tools\ToolsDate;

class RssService
{
    protected const ATOM_FEED = '<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
      xmlns:thr="http://purl.org/syndication/thread/1.0"
      xml:lang="fr">
    <title type="text">%GLOBAL_TITLE%</title>
    <link rel="alternate" type="text/html" href="%URL%" />
    <updated>%DATE%</updated>

    <author>
        <name>%AUTHOR%</name>
    </author>
    <link rel="self" type="application/atom+xml" href="%FEED_URL%" />

    <subtitle type="text">%SUBTITLE%</subtitle>
    <id>%URL%</id>

    %CONTENT%
</feed>';

    protected const ATOM_ENTRY = '<entry xml:lang="fr">
    <id>%URL_DAY%</id>
    <title type="html">%TITLE%</title>
    <updated>%UPDATED_DATE%</updated>

    <author>
        <name>%AUTHOR%</name>
        <uri>%URL_TEMPO%</uri>
    </author>
    <content type="html">
        &lt;h1&gt;%TITLE%&lt;/h1&gt;
        %CONTENT%
    </content>
    <link rel="alternate" type="text/html" href="%URL_DAY%" />
    
    <published>%PUBLISHED_DATE%</published>

</entry>';

    private string $now;

    public function __construct(
        private readonly string $rssFilePath,
        private readonly UrlGeneratorInterface $router,
        private readonly ColorDayService $colorDayService,
    ) {
        $this->now = ToolsDate::getNow(false)->format('Y-m-d\TH:i:s\Z');
    }

    public function generateRss(): void
    {
        $articles = [];
        $colorDays = $this->colorDayService->getAllColorDay();
        $tempoUrl = $this->router->generate('main', [], $this->router::ABSOLUTE_URL);

        foreach ($colorDays as $colorDay) {
            $message = $this->colorDayService->getNotificationMessageOfTheColorDay($colorDay);
            $date = $colorDay->getPublishedAt() ?? $colorDay->getDay();
            $dayUrl = $this->router->generate('day', ['date' => $colorDay->getDay()->format('Y-m-d')], $this->router::ABSOLUTE_URL);

            $template = self::ATOM_ENTRY;
            $template = str_replace('%AUTHOR%', 'Open Tempo', $template);
            $template = str_replace('%TITLE%', $message->getTitle(), $template);
            $template = str_replace('%URL_TEMPO%', $tempoUrl, $template);
            $template = str_replace('%URL_DAY%', $dayUrl, $template);
            $template = str_replace('%PUBLISHED_DATE%', $date->format('Y-m-d\TH:i:s\Z'), $template);
            $template = str_replace('%UPDATED_DATE%', $this->now, $template);
            $template = str_replace('%CONTENT%', $message->getMessage(), $template);
            $template = str_replace('%SUMMARY%', $message->getMessage(), $template);

            $articles[] = $template;
        }

        $feed = $this::ATOM_FEED;
        $feed = str_replace('%GLOBAL_TITLE%', 'Open Tempo RSS', $feed);
        $feed = str_replace('%SUBTITLE%', 'Chaque jour la couleur Tempo directement dans votre lecteur de flux', $feed);
        $feed = str_replace('%DATE%', $this->now, $feed);
        $feed = str_replace('%URL%', $tempoUrl, $feed);
        $feed = str_replace('%FEED_URL%', $tempoUrl.'/open-tempo.xml', $feed);
        $feed = str_replace('%CONTENT%', implode($articles), $feed);
        $feed = str_replace('%AUTHOR%', 'Open Tempo', $feed);

        file_put_contents($this->rssFilePath, $feed);
    }
}
