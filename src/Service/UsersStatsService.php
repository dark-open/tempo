<?php

namespace Tempo\Service;

use Symfony\Contracts\Cache\ItemInterface;
use Tempo\Entity\UsersStats;
use Tempo\Repository\UsersStatsRepository;
use Tempo\Tools\ToolsDate;

class UsersStatsService
{
    public function __construct(
        private readonly UsersStatsRepository $usersStatsRepository,
        private readonly CacheService $cacheService)
    {
    }

    public function addWebStat(?\DateTime $dateTime = null): UsersStats
    {
        $dateTime = $dateTime ?? ToolsDate::getNow(false);

        $cacheId = $this->cacheService->getStatWebId($dateTime);

        return $this->cacheService->get($cacheId, function (ItemInterface $item) use ($dateTime) {
            if ($item->isHit()) {
                return $item;
            }

            $usersStats = $this->getOrCreateDaysStats($dateTime);
            if (is_null($usersStats->getWeb())) {
                $usersStats->setWeb($dateTime);
                $this->usersStatsRepository->save($usersStats, true);
            }

            return $usersStats;
        });
    }

    public function addApiStat(?\DateTime $dateTime = null): UsersStats
    {
        $dateTime = $dateTime ?? ToolsDate::getNow(false);

        $cacheId = $this->cacheService->getStatApiId($dateTime);

        return $this->cacheService->get($cacheId, function (ItemInterface $item) use ($dateTime) {
            if ($item->isHit()) {
                return $item;
            }

            $usersStats = $this->getOrCreateDaysStats($dateTime);
            if (is_null($usersStats->getApi())) {
                $usersStats->setApi($dateTime);
                $this->usersStatsRepository->save($usersStats, true);
            }

            return $usersStats;
        });
    }

    public function addWebPushStats(?\DateTime $dateTime = null): UsersStats
    {
        $dateTime = $dateTime ?? ToolsDate::getNow(false);
        $cacheId = $this->cacheService->getStatWebPushId($dateTime);

        return $this->cacheService->get($cacheId, function (ItemInterface $item) use ($dateTime) {
            if ($item->isHit()) {
                return $item;
            }

            $usersStats = $this->getOrCreateDaysStats($dateTime);
            if (is_null($usersStats->getWebPush())) {
                $usersStats->setWebPush($dateTime);
                $this->usersStatsRepository->save($usersStats, true);
            }

            return $usersStats;
        });
    }

    public function addCalendarStat(?\DateTime $dateTime = null): UsersStats
    {
        $dateTime = $dateTime ?? ToolsDate::getNow(false);
        $cacheId = $this->cacheService->getStatCalendarId($dateTime);

        return $this->cacheService->get($cacheId, function (ItemInterface $item) use ($dateTime) {
            if ($item->isHit()) {
                return $item;
            }

            $usersStats = $this->getOrCreateDaysStats($dateTime);
            if (is_null($usersStats->getCalendar())) {
                $usersStats->setCalendar($dateTime);
                $this->usersStatsRepository->save($usersStats, true);
            }

            return $usersStats;
        });
    }

    public function addRssStats(?\DateTime $dateTime = null): UsersStats
    {
        $dateTime = $dateTime ?? ToolsDate::getNow(false);
        $cacheId = $this->cacheService->getStatRssId($dateTime);

        return $this->cacheService->get($cacheId, function (ItemInterface $item) use ($dateTime) {
            if ($item->isHit()) {
                return $item;
            }

            $usersStats = $this->getOrCreateDaysStats($dateTime);
            if (is_null($usersStats->getCalendar())) {
                $usersStats->setRss($dateTime);
                $this->usersStatsRepository->save($usersStats, true);
            }

            return $usersStats;
        });
    }

    public function saveWebPushNotificationStats(int $total, int $unsubscribes): UsersStats
    {
        $dateTime = ToolsDate::getNow(false);
        $usersStats = $this->getOrCreateDaysStats($dateTime);
        $usersStats
            ->setPushSubscriptions($total)
            ->setPushUnsubscribes($unsubscribes);
        $this->usersStatsRepository->save($usersStats, true);

        return $usersStats;
    }

    /**
     * @return array<int,mixed>
     */
    public function getLastYearStats(): array
    {
        $today = ToolsDate::getNow();
        $lastYear = ToolsDate::getXPastDay(365);

        $cacheId = $this->cacheService->getStatYearId($today);

        return $this->cacheService->get($cacheId, function () use ($lastYear, $today) {
            $stats = $this->usersStatsRepository->getStatsBetween($lastYear, $today);

            $statsArray = [];
            foreach ($stats as $stat) {
                $statsArray[] = $stat->toArray();
            }

            return $statsArray;
        });
    }

    protected function getOrCreateDaysStats(\DateTime $dateTime, bool $flush = false): UsersStats
    {
        $dateTime = \DateTimeImmutable::createFromMutable($dateTime);
        $usersStats = $this->usersStatsRepository->findOneBy(['day' => $dateTime]);
        if (!$usersStats) {
            $usersStats = $this->usersStatsRepository->add($dateTime, $flush);
        }

        return $usersStats;
    }
}
