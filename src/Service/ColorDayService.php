<?php

namespace Tempo\Service;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Tempo\Entity\ColorDay;
use Tempo\Enum\Color;
use Tempo\Object\NotificationMessage;
use Tempo\Repository\ColorDayRepository;
use Tempo\Tools\ToolsColor;
use Tempo\Tools\ToolsDate;

class ColorDayService
{
    public function __construct(
        private readonly ColorDayRepository $colorDayRepository,
        private readonly CacheService $cacheService,
        private readonly PricesService $pricesService,
        private readonly LoggerInterface $prodLogger,
    ) {
    }

    public function getColorDay(\DateTime $dateTime, bool $ignoreCache = false): ColorDay
    {
        return $this->getOrCreateColorDay($dateTime, $ignoreCache, true);
    }

    /** @return array<string,bool|\DateTime|ColorDay|null> */
    public function getColorDaySecured(\DateTime $date): array
    {
        $securedData = [
            'success' => false,
            'min' => null,
            'max' => ToolsDate::getNextDay(),
            'colorDay' => null,
        ];

        try {
            $oldestColorDate = $this->getOldestDay();
        } catch (NoResultException|NonUniqueResultException $e) {
            $this->prodLogger->error($e);

            $securedData['closerDate'] = ToolsDate::getNow();

            return $securedData;
        }

        $securedData['min'] = $oldestColorDate->getDay();

        $dateFutureSecured = ToolsDate::getSmaller($date, ToolsDate::getNextDay());
        if ($date->format('Ymd') !== $dateFutureSecured->format('Ymd')) {
            $this->prodLogger->warning('Date trop loin dans le futur');

            return $securedData;
        }

        $datePastSecured = ToolsDate::getSmaller($oldestColorDate->getDay(), $date);
        if ($oldestColorDate->getDay()->format('Ymd') !== $datePastSecured->format('Ymd')) {
            $this->prodLogger->warning('Date trop loin dans le passé, la plus ancienne date est '.$datePastSecured->format('Y-m-d'));

            return $securedData;
        }

        $securedData['colorDay'] = $this->getColorDay($date);
        $securedData['success'] = true;

        return $securedData;
    }

    /**
     * @return array<int,ColorDay>
     */
    public function getTodayAndTomorrowColor(ColorDayService $colorDayService): array
    {
        $today = ToolsDate::getNow();
        $tomorrow = ToolsDate::getNextDay($today);

        $todayColor = $colorDayService->getColorDay($today);
        $tomorrowColor = $colorDayService->getColorDay($tomorrow);

        return [$todayColor, $tomorrowColor];
    }

    /**
     * @throws \Exception
     */
    public function setOrUpdateColorOfTheDay(\DateTime $dateTime, Color $color, bool $ignoreCache = false): ColorDay
    {
        $colorDay = $this->getOrCreateColorDay($dateTime, $ignoreCache);
        $colorDay->setColor($color);
        $colorDay->setPublishedAt(new \DateTimeImmutable());
        $colorDay->setValidated(ToolsDate::isAfterValidatedTime() && Color::Undefined !== $colorDay->getColor());
        $this->pricesService->setPriceToColorDay($colorDay);

        $this->colorDayRepository->flush();

        return $colorDay;
    }

    public function deleteColorDayCache(\DateTime $dateTime): void
    {
        $this->cacheService->deleteColorDayCache($dateTime);
    }

    /**
     * @return ColorDay[]
     */
    public function getAllColorDayOrdered(): array
    {
        return $this->colorDayRepository->findAllOrdered();
    }

    protected function getOldestDay(): ColorDay
    {
        $cacheId = $this->cacheService->getOldestColorDayId();

        return $this->cacheService->get($cacheId, function (ItemInterface $item) {
            if ($item->isHit()) {
                return $item;
            }

            return $this->colorDayRepository->getOldestDay();
        });
    }

    public function getNotificationMessageOfTheColorDay(ColorDay $colorDay): NotificationMessage
    {
        $dateText = ToolsDate::frenchDate($colorDay->getDay());
        $title = ToolsColor::getEmojiFromColor($colorDay->getColor()).' '.$dateText;
        $message = 'Le '.$dateText.' est un jour '.ToolsColor::getFrenchTextFromColor($colorDay->getColor()).'.';

        if (Color::Undefined !== $colorDay->getColor()) {
            $blueDaysStr = $colorDay->getBlueLeft() > 1 ? 'jours bleus' : 'jour bleu';
            $whiteDaysStr = $colorDay->getWhiteLeft() > 1 ? 'jours blancs' : 'jour blanc';
            $redDaysStr = $colorDay->getRedLeft() > 1 ? 'jours rouges' : 'jour rouge';
            $message .= ' Il reste '.$colorDay->getBlueLeft().' '.$blueDaysStr.', ';
            $message .= $colorDay->getWhiteLeft().' '.$whiteDaysStr.' et ';
            $message .= $colorDay->getRedLeft().' '.$redDaysStr;
            $message .= ' jusqu\'au 31 aout.';
        }

        return new NotificationMessage($title, $message);
    }

    /**
     * @return array<int,ColorDay>
     */
    public function getAllColorDay(): array
    {
        return $this->colorDayRepository->findBy([], ['day' => 'ASC']);
    }

    protected function getOrCreateColorDay(\DateTime $dateTime, bool $ignoreCache = false, bool $flush = false): ColorDay
    {
        $dateTime->setTime(0, 0);

        if ($ignoreCache) {
            $colorDay = $this->colorDayRepository->findOneBy(['day' => $dateTime]);

            if (!$colorDay) {
                $colorDay = $this->colorDayRepository->add($dateTime, true);
            }

            return $colorDay;
        }

        $cacheId = $this->cacheService->getColorDayId($dateTime);

        return $this->cacheService->get($cacheId, function (ItemInterface $item) use ($dateTime) {
            if ($item->isHit()) {
                return $item;
            }

            $colorDay = $this->colorDayRepository->findOneBy(['day' => $dateTime]);

            if (!$colorDay) {
                $colorDay = $this->colorDayRepository->add($dateTime);
                $colorDay->setValidated(ToolsDate::isAfterValidatedTime() && Color::Undefined !== $colorDay->getColor());
                $this->colorDayRepository->flush();
            }

            return $colorDay;
        });
    }
}
