<?php

namespace Tempo\Service;

use Tempo\Entity\ColorDay;
use Tempo\Entity\Prices;
use Tempo\Repository\PricesRepository;

class PricesService
{
    /** @var array|Prices[] */
    private array $prices;

    public function __construct(
        private readonly PricesRepository $pricesRepository,
    ) {
        $this->prices = $this->pricesRepository->findAllOrderedInverse();
    }

    public function setPriceToColorDay(ColorDay $colorDay): void
    {
        /** @var Prices $price */
        foreach ($this->prices as $price) {
            if ($price->getStartAt() <= $colorDay->getDay()) {
                $colorDay->setPrices($price);
                $price->addColorDay($colorDay);

                break;
            }
        }
    }
}
