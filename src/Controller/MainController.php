<?php

namespace Tempo\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tempo\Entity\WebPushSubscription;
use Tempo\Form\WebPushSubscriptionType;
use Tempo\Service\CalendarService;
use Tempo\Service\ColorDayService;
use Tempo\Service\MessageService;
use Tempo\Service\UsersStatsService;
use Tempo\Service\WebPushSubscriptionService;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(
        UsersStatsService $usersStatsService,
        ColorDayService $colorDayService,
        MessageService $messageService,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $messages = $messageService->getMessages();
        $usersStatsService->addWebStat();

        return $this->render('client/index.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
            'messages' => $messages,
        ]);
    }

    #[Route('/low', name: 'main_low')]
    public function low(
        ColorDayService $colorDayService,
        MessageService $messageService,
        Request $request,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $messages = $messageService->getMessages();
        $theme = $request->get('theme') ?? 'light';

        return $this->render('low/index.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
            'messages' => $messages,
            'darkTheme' => ('dark' === $theme),
        ]);
    }

    #[Route('/day/{date}', name: 'day', requirements: ['date' => '\d{4}-\d{2}-\d{2}'])]
    #[ParamConverter('date', options: ['format' => 'Y-m-d'])]
    public function day(
        ColorDayService $colorDayService,
        UsersStatsService $usersStatsService,
        \DateTime $date = new \DateTime(),
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $dataSecured = $colorDayService->getColorDaySecured($date);

        if (!$dataSecured['success']) {
            $this->addFlash('warning', 'La date '.$date->format('Y-m-d').' ne peut être affichée.');
            $this->addFlash('info', 'La date la plus ancienne disponible est : '.$dataSecured['min']->format('Y-m-d'));
            $this->addFlash('info', 'la date la plus éloignée dans le futur est : '.$dataSecured['max']->format('Y-m-d'));

            return $this->redirectToRoute('day', ['date' => $dataSecured['max']->format('Y-m-d')]);
        }

        $usersStatsService->addWebStat();

        return $this->render('client/day.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
            'dayColor' => $dataSecured['colorDay'],
        ]);
    }

    #[Route('/webpush', name: 'web_push')]
    public function webPush(
        ColorDayService $colorDayService,
        UsersStatsService $usersStatsService,
        WebPushSubscriptionService $webPushSubscriptionService,
        Request $request,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);

        $form = $this->createForm(WebPushSubscriptionType::class, new WebPushSubscription());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $subscription = $webPushSubscriptionService->addNewSubscription($form->getData());
            try {
                $webPushSubscriptionService->sendWelcomeNotification($subscription);
            } catch (\ErrorException $e) {
                $this->addFlash('error', $e->getMessage());
            }

            $usersStatsService->addWebPushStats();
        }

        return $this->render('client/web-push.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
            'form' => $form->createView(),
        ]);
    }

    #[Route('/calendar', name: 'calendar')]
    public function calendar(
        ColorDayService $colorDayService,
        UsersStatsService $usersStatsService,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $usersStatsService->addCalendarStat();

        return $this->render('client/calendar.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
        ]);
    }

    #[Route('/rss', name: 'rss')]
    public function rss(
        ColorDayService $colorDayService,
        UsersStatsService $usersStatsService,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $usersStatsService->addRssStats();

        return $this->render('client/rss.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
        ]);
    }

    #[Route('/open-tempo.xml', name: 'rss_feed')]
    public function rssFeed(
    ): void {
    }

    #[Route('/tempo-ics', name: 'ics')]
    public function ics(
        CalendarService $calendarService,
    ): Response {
        $icalContent = $calendarService->publishCalendar();

        return new Response($icalContent, 200, [
            'Content-Type' => 'text/calendar',
        ]);
    }

    #[Route('/cgu', name: 'cgu')]
    public function cgu(
        ColorDayService $colorDayService,
        UsersStatsService $usersStatsService,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $usersStatsService->addWebStat();

        return $this->render('client/cgu.html.twig', [
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
        ]);
    }
}
