<?php

namespace Tempo\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Tempo\Service\ColorDayService;
use Tempo\Service\MessageService;
use Tempo\Service\UsersStatsService;

#[Route('/api')]
class ApiController extends AbstractController
{
    public function __construct(private readonly string $webPushPublicKey)
    {
    }

    #[Route('/', name: 'api_main')]
    #[Route('/doc', name: 'api_doc')]
    public function jsonDoc(
        ColorDayService $colorDayService,
    ): Response {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);

        return $this->render('client/api-doc.html.twig', [
            'controller_name' => 'MainController',
            'todayColor' => $data[0],
            'tomorrowColor' => $data[1],
        ]);
    }

    #[Route('/now', name: 'api_now')]
    public function jsonNow(
        UsersStatsService $usersStatsService,
        ColorDayService $colorDayService,
        MessageService $messageService,
    ): JsonResponse {
        $data = $colorDayService->getTodayAndTomorrowColor($colorDayService);
        $messages = $messageService->getMessages(null, true);

        $dataJson = ['messages' => $messages, $data[0]->toArray(), $data[1]->toArray()];
        $usersStatsService->addApiStat();

        return new JsonResponse($dataJson, 200, ['Content-Type' => 'application/json;']);
    }

    #[Route('/day/{date}', name: 'api_day', requirements: ['date' => '\d{4}-\d{2}-\d{2}'])]
    #[ParamConverter('date', options: ['format' => 'Y-m-d'])]
    public function jsonDate(
        UsersStatsService $usersStatsService,
        ColorDayService $colorDayService,
        MessageService $messageService,
        \DateTime $date = new \DateTime(),
    ): JsonResponse {
        $usersStatsService->addApiStat();

        $dataSecured = $colorDayService->getColorDaySecured($date);

        if (!$dataSecured['success']) {
            $data = [
                'message' => 'La date '.$date->format('Y-m-d').' ne peut être affichée.',
                '@see' => [
                    'min' => $this->generateUrl('api_day', ['date' => $dataSecured['min']->format('Y-m-d')], UrlGeneratorInterface::ABSOLUTE_PATH),
                    'max' => $this->generateUrl('api_day', ['date' => $dataSecured['max']->format('Y-m-d')], UrlGeneratorInterface::ABSOLUTE_PATH)],
            ];

            return new JsonResponse($data, 400, ['Content-Type' => 'application/json;']);
        }

        $messages = $messageService->getMessages($date, true);
        $dataJson = ['messages' => $messages, 'data' => [$dataSecured['colorDay']->toArray()]];

        return new JsonResponse($dataJson, 200, ['Content-Type' => 'application/json;']);
    }

    #[Route('/stats/users', name: 'api_stats_users')]
    public function annualStats(UsersStatsService $usersStatsService): JsonResponse
    {
        $data = $usersStatsService->getLastYearStats();

        return new JsonResponse($data);
    }

    #[Route('/notifications/web/key', name: 'api_notification-web_key', methods: ['get'])]
    public function notificationWebGetKey(): JsonResponse
    {
        return new JsonResponse(['key' => $this->webPushPublicKey], Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/notifications/web/subscribe', name: 'api_notification-web_subscribe', methods: ['post'])]
    public function notificationWebSubscribe(): void
    {
    }
}
