<?php

namespace Tempo\Object;

class CommandReport
{
    public function __construct(
        private bool $isSuccess,
        private string $message)
    {
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function setIsSuccess(bool $isSuccess): CommandReport
    {
        $this->isSuccess = $isSuccess;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): CommandReport
    {
        $this->message = $message;

        return $this;
    }
}
