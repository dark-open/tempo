<?php

namespace Tempo\Object;

class NotificationMessage
{
    public function __construct(private string $title, private string $message)
    {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): NotificationMessage
    {
        $this->title = $title;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): NotificationMessage
    {
        $this->message = $message;

        return $this;
    }
}
