<?php

namespace Tempo\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tempo\Entity\WebPushSubscription;

class WebPushSubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('endPoint', HiddenType::class)
            ->add('publicKey', HiddenType::class)
            ->add('authToken', HiddenType::class)
            ->add('contentEncoding', HiddenType::class, [
                'data' => 'nothing',
            ])
            ->add('isBlue', CheckboxType::class, [
                'label' => 'Jours bleus',
                'data' => true,
            ])
            ->add('isWhite', CheckboxType::class, [
                'label' => 'Jours blancs',
                'data' => true,
            ])
            ->add('isRed', CheckboxType::class, [
                'label' => 'Jours rouges',
                'data' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Recevoir les notifications web',
                'attr' => [
                    'class' => 'btn btn-green',
                ],
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
            $data = $event->getData();

            if (!isset($data['isBlue']) && !isset($data['isWhite']) && !isset($data['isRed'])) {
                $event->getForm()->addError(new FormError('Vous devez choisir au moins une couleur.'));
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WebPushSubscription::class,
        ]);
    }
}
