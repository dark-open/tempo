<?php

namespace Tempo\Twig;

use Tempo\Tools\ToolsColor;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TempoExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_env', [$this, 'getEnv']),
            new TwigFunction('color_day_hash', [$this, 'getColorDayHash']),
            new TwigFunction('french_trans_color', [$this, 'getFrenchTransColor']),
            new TwigFunction('emoji', [$this, 'getEmoji']),
        ];
    }

    public function getEnv(string $varname): string
    {
        return $_ENV[$varname] ?? '';
    }

    public function getColorDayHash(string $char): string
    {
        return ToolsColor::getEnglishTextFromChar($char);
    }

    public function getFrenchTransColor(string $english): string
    {
        return match ($english) {
            'blue' => 'bleu',
            'white' => 'blanc',
            'red' => 'rouge',
            default => 'inconnu',
        };
    }

    public function getEmoji(string $char): string
    {
        return ToolsColor::getEmojiFromValue($char);
    }
}
