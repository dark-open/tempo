<?php

namespace Tempo\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tempo\Entity\UsersStats;

/**
 * @extends ServiceEntityRepository<UsersStats>
 *
 * @method UsersStats|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersStats|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersStats[]    findAll()
 * @method UsersStats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersStatsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersStats::class);
    }

    public function add(\DateTimeInterface $dateTime, bool $flush = false): UsersStats
    {
        $usersStats = new UsersStats();
        $usersStats->setDay($dateTime);
        $this->getEntityManager()->persist($usersStats);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $usersStats;
    }

    public function save(UsersStats $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UsersStats $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return array<int,UsersStats>
     */
    public function getStatsBetween(\DateTime $startDate, \DateTime $endDate): array
    {
        $qb = $this->createQueryBuilder('q');
        $qb
            ->where($qb->expr()->gte('q.day', ':startDate'))
            ->andWhere($qb->expr()->lt('q.day', ':endDate'))
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->orderBy('q.day', 'asc');

        return $qb->getQuery()->execute();
    }
}
