<?php

namespace Tempo\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tempo\Entity\Message;

/**
 * @extends ServiceEntityRepository<Message>
 *
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function save(Message $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Message $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Message[]
     */
    public function getAllMessagesToDisplayToday(\DateTime $date): array
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->where($qb->expr()->lte('m.startAt', ':startAt'))
            ->andWhere($qb->expr()->gte('m.endAt', ':endAt'))
            ->setParameter('startAt', $date)
            ->setParameter('endAt', $date)
            ->orderBy('m.startAt', 'ASC');

        /** @var array<int,Message> $data */
        $data = $qb->getQuery()->execute();

        return $data;
    }
}
