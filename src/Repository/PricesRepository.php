<?php

namespace Tempo\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tempo\Entity\Prices;

/**
 * @extends ServiceEntityRepository<Prices>
 */
class PricesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prices::class);
    }

    /**
     * @return Prices[]
     */
    public function findAllOrderedInverse(): array
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.startAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
