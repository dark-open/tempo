<?php

namespace Tempo\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tempo\Entity\WebPushSubscription;
use Tempo\Enum\Color;

/**
 * @extends ServiceEntityRepository<WebPushSubscription>
 *
 * @method WebPushSubscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method WebPushSubscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method WebPushSubscription[]    findAll()
 * @method WebPushSubscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebPushSubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebPushSubscription::class);
    }

    public function save(WebPushSubscription $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(WebPushSubscription $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return array<int,WebPushSubscription>
     *
     * @throws \Exception
     */
    public function getSubscriptionByColor(Color $color): array
    {
        $qb = $this->createQueryBuilder('wps');

        $element = match ($color->value) {
            'B' => 'isBlue',
            'W' => 'isWhite',
            'R' => 'isRed',
            default => throw new \Exception('La couleur de demain n\'est pas encore définie'),
        };

        $qb
            ->where($qb->expr()->eq('wps.'.$element, ':true'))
            ->setParameter('true', true);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array<int,string> $reports
     */
    public function removeUnsubscribedPeople(array $reports): void
    {
        $qb = $this->createQueryBuilder('wps');

        $qb->delete(WebPushSubscription::class, 'wps')
            ->where($qb->expr()->in('wps.authToken', ':reports'))
            ->setParameter('reports', $reports);

        $query = $qb->getQuery();
        $query->execute();
    }
}
