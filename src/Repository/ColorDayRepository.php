<?php

namespace Tempo\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Tempo\Entity\ColorDay;

/**
 * @extends ServiceEntityRepository<ColorDay>
 *
 * @method ColorDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method ColorDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method ColorDay[]    findAll()
 * @method ColorDay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ColorDayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ColorDay::class);
    }

    public function add(\DateTime $dateTime, bool $flush = false): ColorDay
    {
        $colorDay = new ColorDay();
        $colorDay->setDay($dateTime);
        $this->getEntityManager()->persist($colorDay);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $colorDay;
    }

    public function save(ColorDay $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ColorDay $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }

    public function getOldestDay(): ?ColorDay
    {
        $qb = $this->createQueryBuilder('cd');

        $qb
            ->orderBy('cd.day', 'ASC')
            ->setMaxResults(1);

        try {
            $obj = $qb->getQuery()->getSingleResult(AbstractQuery::HYDRATE_OBJECT);
        } catch (NoResultException|NonUniqueResultException $e) {
            return null;
        }

        return $obj;
    }

    /**
     * @return float|int|mixed|string
     */
    public function findAllOrdered()
    {
        return $this->createQueryBuilder('cd')
            ->orderBy('cd.day', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
