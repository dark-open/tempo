<?php

namespace Tempo\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tempo\Service\CommandService;

#[AsCommand(
    name: 'tempo:notification:tomorrow',
    description: 'Trigger the send of web notification for the color of tomorrow',
)]
class TempoNotificationOfTheDayCommand extends Command
{
    public function __construct(
        private readonly CommandService $commandService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $commandReport = $this->commandService->sendNotificationOfTheDay();

        if (!$commandReport->isSuccess()) {
            $io->error($commandReport->getMessage());

            return Command::FAILURE;
        }

        $io->success($commandReport->getMessage());

        return Command::SUCCESS;
    }
}
