<?php

namespace Tempo\Command;

use Minishlink\WebPush\VAPID;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'tempo:vapid:generate',
    description: 'generate and print pair of vapid key in the console'
)]
class GenerateVapidKeyCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->writeln('Generate pair of keys : ');

        try {
            $keys = VAPID::createVapidKeys();
        } catch (\ErrorException $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }

        $io->writeln('Public key : '.$keys['publicKey']);
        $io->writeln('Private key : '.$keys['privateKey']);

        return Command::SUCCESS;
    }
}
