<?php

namespace Tempo\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tempo\Entity\ColorDay;
use Tempo\Enum\Color;
use Tempo\Exception\ApiException;
use Tempo\Repository\ColorDayRepository;
use Tempo\Service\RTEApiService;
use Tempo\Tools\ToolsDate;

#[AsCommand(
    name: 'tempo:init',
    description: 'Init the database with days from 1st september of the Tempo year',
)]
class InitDataBaseCommand extends Command
{
    public function __construct(
        private readonly ColorDayRepository $colorDayRepository,
        private readonly RTEApiService $RTEApiService,
        private readonly EntityManagerInterface $em,
    ) {
        parent::__construct();
    }

    /**
     * @throws ApiException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $oldestDay = $this->colorDayRepository->getOldestDay();
        $now = ToolsDate::getNow();
        $lastDay = clone $now;
        $firstDay = ToolsDate::getPastFirstOfSeptember($lastDay);
        $tempDay = $firstDay;
        $tempoYearIsLeap = $firstDay->format('Y') !== $now->format('Y') ? ToolsDate::isLeapYear(intval($now->format('Y'))) : ToolsDate::isLeapYear(intval($firstDay->format('Y')));
        $tomorrow = ToolsDate::getNextDay();
        $nowTime = ToolsDate::getNow(false);

        //        if ($oldestDay && ToolsDate::isFirstOfSeptember($oldestDay->getDay())) {
        //            $io->success('Nothing to init, the database starts a first of september');
        //
        //            return Command::SUCCESS;
        //        }

        $all = $this->colorDayRepository->findAll();
        foreach ($all as $day) {
            $this->em->remove($day);
        }

        $blueLeft = $tempoYearIsLeap ? ColorDay::BLUE_DAYS + 1 : ColorDay::BLUE_DAYS;
        $whiteLeft = ColorDay::WHITE_DAYS;
        $redLeft = ColorDay::RED_DAYS;
        $thirtyDaysInterval = new \DateInterval('P30D');

        while ($tempDay->getTimestamp() <= $lastDay->getTimestamp()) {
            $tempEndDay = (clone $tempDay)->add($thirtyDaysInterval);
            if ($tempEndDay->getTimestamp() > $tomorrow->getTimestamp()) {
                $tempEndDay = clone $tomorrow;
            }
            $io->info('From '.$tempDay->format('Y-m-d').' to '.$tempEndDay->format('Y-m-d'));

            $days = $this->RTEApiService->getColorByDates($tempDay, $tempEndDay);
            foreach ($days as $day) {
                $colorDay = $this->colorDayRepository->findOneBy(['day' => $day['day']]);
                if (!$colorDay) {
                    $colorDay = new ColorDay();
                    $colorDay
                        ->setDay($day['day'])
                        ->setColor(Color::getColorFromString($day['color']))
                        ->setValidated(true)
                        ->setPublishedAt(\DateTimeImmutable::createFromMutable($nowTime));
                    $this->em->persist($colorDay);
                }

                if (Color::Blue === $colorDay->getColor()) {
                    --$blueLeft;
                } elseif (Color::White === $colorDay->getColor()) {
                    --$whiteLeft;
                } elseif (Color::Red === $colorDay->getColor()) {
                    --$redLeft;
                } else {
                    if (ToolsDate::isSunday($day['day'])) {
                        $colorDay->setColor(Color::Blue);
                        --$blueLeft;
                    } elseif ($day['day']->getTimestamp() > $lastDay->getTimestamp()) {
                        break;
                    }
                }

                $colorDay
                    ->setBlueLeft($blueLeft)
                    ->setWhiteLeft($whiteLeft)
                    ->setRedLeft($redLeft);
            }

            $this->em->flush();
            $this->em->clear();
            $tempDay = $tempEndDay;
        }
        $io->success('Database init with success');

        return Command::SUCCESS;
    }
}
