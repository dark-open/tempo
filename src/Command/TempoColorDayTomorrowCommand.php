<?php

namespace Tempo\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tempo\Service\CommandService;
use Tempo\Tools\ToolsDate;

#[AsCommand(
    name: 'tempo:color-day:tomorrow',
    description: 'Get the color of tomorrow from RTE API and save it in DB',
)]
class TempoColorDayTomorrowCommand extends Command
{
    public function __construct(
        private readonly CommandService $commandService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $today = ToolsDate::getNow();
        $tomorrow = ToolsDate::getNextDay($today);

        $commandReport = $this->commandService->setColorOfTheDayByDate($tomorrow);

        if ($commandReport->isSuccess()) {
            $this->commandService->generateRss();
        }
        if (!$commandReport->isSuccess()) {
            $io->error($commandReport->getMessage());

            return Command::FAILURE;
        }

        $io->success($commandReport->getMessage());

        return Command::SUCCESS;
    }
}
