<?php

namespace Tempo\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tempo\Service\CommandService;

#[AsCommand(
    name: 'tempo:calendar:generate',
    description: 'Add a new event corresponding of tomorrow in the iCalendar file',
)]
class CalendarCommand extends Command
{
    public function __construct(
        private readonly CommandService $commandService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $commandReport = $this->commandService->addNewEventInCalendar('./public/');

        if (!$commandReport->isSuccess()) {
            $io->error($commandReport->getMessage());

            return Command::FAILURE;
        }

        $io->success($commandReport->getMessage());

        return Command::SUCCESS;
    }
}
