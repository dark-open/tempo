<?php

namespace Tempo\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Tempo\Repository\PricesRepository;

#[ORM\Entity(repositoryClass: PricesRepository::class)]
class Prices
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $startAt = null;

    #[ORM\Column]
    private ?int $blueLowPrice = null;

    #[ORM\Column]
    private ?int $blueHighPrice = null;

    #[ORM\Column]
    private ?int $whiteLowPrice = null;

    #[ORM\Column]
    private ?int $whiteHighPrice = null;

    #[ORM\Column]
    private ?int $redLowPrice = null;

    #[ORM\Column]
    private ?int $redHighPrice = null;

    /**
     * @var Collection<int, ColorDay>
     */
    #[ORM\OneToMany(mappedBy: 'prices', targetEntity: ColorDay::class)]
    private Collection $colorDays;

    public function __construct()
    {
        $this->colorDays = new ArrayCollection();
    }

    /**
     * @return array<string,string>
     */
    public function toArray(): array
    {
        return [
            'startAt' => $this->startAt->format('Y-m-d H:i:s\Z'),
            'blueLowPrice' => $this->blueLowPrice / 10000,
            'blueHighPrice' => $this->blueHighPrice / 10000,
            'whiteLowPrice' => $this->whiteLowPrice / 10000,
            'whiteHighPrice' => $this->whiteHighPrice / 10000,
            'redLowPrice' => $this->redLowPrice / 10000,
            'redHighPrice' => $this->redHighPrice / 10000,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeImmutable $startAt): static
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getBlueLowPrice(): ?int
    {
        return $this->blueLowPrice;
    }

    public function setBlueLowPrice(int $blueLowPrice): static
    {
        $this->blueLowPrice = $blueLowPrice;

        return $this;
    }

    public function getBlueHighPrice(): ?int
    {
        return $this->blueHighPrice;
    }

    public function setBlueHighPrice(int $blueHighPrice): static
    {
        $this->blueHighPrice = $blueHighPrice;

        return $this;
    }

    public function getWhiteLowPrice(): ?int
    {
        return $this->whiteLowPrice;
    }

    public function setWhiteLowPrice(int $whiteLowPrice): static
    {
        $this->whiteLowPrice = $whiteLowPrice;

        return $this;
    }

    public function getWhiteHighPrice(): ?int
    {
        return $this->whiteHighPrice;
    }

    public function setWhiteHighPrice(int $whiteHighPrice): static
    {
        $this->whiteHighPrice = $whiteHighPrice;

        return $this;
    }

    public function getRedLowPrice(): ?int
    {
        return $this->redLowPrice;
    }

    public function setRedLowPrice(int $redLowPrice): static
    {
        $this->redLowPrice = $redLowPrice;

        return $this;
    }

    public function getRedHighPrice(): ?int
    {
        return $this->redHighPrice;
    }

    public function setRedHighPrice(int $redHighPrice): static
    {
        $this->redHighPrice = $redHighPrice;

        return $this;
    }

    /**
     * @return Collection<int, ColorDay>
     */
    public function getColorDays(): Collection
    {
        return $this->colorDays;
    }

    public function addColorDay(ColorDay $colorDay): static
    {
        if (!$this->colorDays->contains($colorDay)) {
            $this->colorDays->add($colorDay);
            $colorDay->setPrices($this);
        }

        return $this;
    }

    public function removeColorDay(ColorDay $colorDay): static
    {
        if ($this->colorDays->removeElement($colorDay)) {
            // set the owning side to null (unless already changed)
            if ($colorDay->getPrices() === $this) {
                $colorDay->setPrices(null);
            }
        }

        return $this;
    }
}
