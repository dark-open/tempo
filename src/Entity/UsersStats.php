<?php

namespace Tempo\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tempo\Repository\UsersStatsRepository;

#[ORM\Entity(repositoryClass: UsersStatsRepository::class)]
#[ORM\Table(name: 'tempo_users_stats')]
class UsersStats
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_IMMUTABLE)]
    private \DateTimeImmutable $day;

    #[ORM\Column(type: Types::TIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $web = null;

    #[ORM\Column(type: Types::TIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $api = null;

    #[ORM\Column(type: Types::TIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $calendar = null;

    #[ORM\Column(type: Types::TIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $rss = null;

    #[ORM\Column(type: Types::TIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $webPush = null;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    private int $pushSubscriptions = 0;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    private int $pushUnsubscribes = 0;

    public function __construct()
    {
        $this->day = new \DateTimeImmutable();
    }

    /**
     * @return array<string,string|array<string,string>>
     */
    public function toArray(): array
    {
        return [
            'day' => $this->day->format('Y-m-d'),
            'first_access' => [
                'web' => $this->web?->format('H:i:s'),
                'api' => $this->api?->format('H:i:s'),
                'rss' => $this->rss?->format('H:i:s'),
                'cal' => $this->calendar?->format('H:i:s'),
                'web_push' => $this->webPush?->format('H:i:s'),
            ],
            'web_push_notification' => [
                'total' => $this->pushSubscriptions,
                'unsubscribes' => $this->pushUnsubscribes,
            ],
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): \DateTimeImmutable
    {
        return $this->day;
    }

    public function setDay(\DateTimeInterface $day): self
    {
        if ($day instanceof \DateTime) {
            $day = \DateTimeImmutable::createFromMutable($day);
        }
        $this->day = $day;

        return $this;
    }

    public function getWeb(): ?\DateTimeInterface
    {
        return $this->web;
    }

    public function setWeb(\DateTimeInterface $web): self
    {
        if ($web instanceof \DateTime) {
            $web = \DateTimeImmutable::createFromMutable($web);
        }
        $this->web = $web;

        return $this;
    }

    public function getApi(): ?\DateTimeInterface
    {
        return $this->api;
    }

    public function setApi(\DateTimeInterface $api): self
    {
        if ($api instanceof \DateTime) {
            $api = \DateTimeImmutable::createFromMutable($api);
        }
        $this->api = $api;

        return $this;
    }

    public function getCalendar(): ?\DateTimeInterface
    {
        return $this->calendar;
    }

    public function setCalendar(\DateTimeInterface $calendar): self
    {
        if ($calendar instanceof \DateTime) {
            $calendar = \DateTimeImmutable::createFromMutable($calendar);
        }
        $this->calendar = $calendar;

        return $this;
    }

    public function getRss(): ?\DateTimeInterface
    {
        return $this->rss;
    }

    public function setRss(\DateTimeInterface $rss): self
    {
        if ($rss instanceof \DateTime) {
            $rss = \DateTimeImmutable::createFromMutable($rss);
        }
        $this->rss = $rss;

        return $this;
    }

    public function getWebPush(): ?\DateTimeInterface
    {
        return $this->webPush;
    }

    public function setWebPush(?\DateTimeInterface $webPush): void
    {
        if ($webPush instanceof \DateTime) {
            $webPush = \DateTimeImmutable::createFromMutable($webPush);
        }
        $this->webPush = $webPush;
    }

    public function getPushSubscriptions(): int
    {
        return $this->pushSubscriptions;
    }

    public function setPushSubscriptions(int $pushSubscriptions): UsersStats
    {
        $this->pushSubscriptions = $pushSubscriptions;

        return $this;
    }

    public function getPushUnsubscribes(): int
    {
        return $this->pushUnsubscribes;
    }

    public function setPushUnsubscribes(int $pushUnsubscribes): UsersStats
    {
        $this->pushUnsubscribes = $pushUnsubscribes;

        return $this;
    }
}
