<?php

namespace Tempo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Minishlink\WebPush\SubscriptionInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Tempo\Repository\WebPushSubscriptionRepository;

#[ORM\Entity(repositoryClass: WebPushSubscriptionRepository::class)]
#[ORM\Table(name: 'tempo_web_push_subscription')]
class WebPushSubscription implements SubscriptionInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: false)]
    #[Assert\NotBlank]
    private string $endPoint;

    #[ORM\Column(length: 255, nullable: false)]
    #[Assert\NotBlank]
    private string $publicKey;

    #[ORM\Column(length: 255, nullable: false)]
    #[Assert\NotBlank]
    private string $authToken;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contentEncoding = null;

    #[ORM\Column(nullable: false)]
    private bool $isBlue;

    #[ORM\Column(nullable: false)]
    private bool $isWhite;

    #[ORM\Column(nullable: false)]
    private bool $isRed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEndPoint(): string
    {
        return $this->endPoint;
    }

    public function setEndPoint(string $endPoint): static
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    public function setPublicKey(string $publicKey): static
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    public function getAuthToken(): string
    {
        return $this->authToken;
    }

    public function setAuthToken(string $authToken): static
    {
        $this->authToken = $authToken;

        return $this;
    }

    public function getContentEncoding(): string
    {
        if (is_null($this->contentEncoding)) {
            $this->contentEncoding = '';
        }

        return $this->contentEncoding;
    }

    public function setContentEncoding(string $contentEncoding): static
    {
        $this->contentEncoding = $contentEncoding;

        return $this;
    }

    public function isIsBlue(): bool
    {
        return $this->isBlue;
    }

    public function setIsBlue(bool $isBlue): static
    {
        $this->isBlue = $isBlue;

        return $this;
    }

    public function isIsWhite(): bool
    {
        return $this->isWhite;
    }

    public function setIsWhite(bool $isWhite): static
    {
        $this->isWhite = $isWhite;

        return $this;
    }

    public function isIsRed(): bool
    {
        return $this->isRed;
    }

    public function setIsRed(bool $isRed): static
    {
        $this->isRed = $isRed;

        return $this;
    }
}
