<?php

namespace Tempo\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tempo\Enum\Color;
use Tempo\Repository\ColorDayRepository;

#[ORM\Entity(repositoryClass: ColorDayRepository::class)]
#[ORM\Table(name: 'tempo_color_day')]
class ColorDay
{
    public const BLUE_DAYS = 300;
    public const WHITE_DAYS = 43;
    public const RED_DAYS = 22;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, unique: true)]
    private \DateTime $day;

    #[ORM\Column(type: Types::STRING, length: 1, enumType: Color::class)]
    private Color $color;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $publishedAt = null;

    #[ORM\Column(nullable: false)]
    private bool $validated;

    #[ORM\Column(nullable: true)]
    private ?int $blueLeft = null;

    #[ORM\Column(nullable: true)]
    private ?int $redLeft = null;

    #[ORM\Column(nullable: true)]
    private ?int $whiteLeft = null;

    #[ORM\ManyToOne(inversedBy: 'colorDays')]
    private ?Prices $prices = null;

    public function __construct()
    {
        $this->color = Color::Undefined;
        $this->validated = false;
    }

    /**
     * @return array<string,string>
     */
    public function toArray(): array
    {
        return [
            'day' => $this->day->format('Y-m-d H:i:s\Z'),
            'color' => $this->color->value,
            'blue_left' => $this->blueLeft,
            'white_left' => $this->whiteLeft,
            'red_left' => $this->redLeft,
            'prices' => $this->prices->toArray(),
        ];
    }

    public function getUsedOnTotal(): string
    {
        return match ($this->color) {
            Color::Blue => (!is_null($this->blueLeft)) ? (self::BLUE_DAYS - $this->blueLeft).' / '.self::BLUE_DAYS : '',
            Color::White => (!is_null($this->whiteLeft)) ? (self::WHITE_DAYS - $this->whiteLeft).' / '.self::WHITE_DAYS : '',
            Color::Red => (!is_null($this->redLeft)) ? (self::RED_DAYS - $this->redLeft).' / '.self::RED_DAYS : '',
            default => '',
        };
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): \DateTime
    {
        return $this->day;
    }

    public function setDay(\DateTime $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function setColor(Color $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeImmutable $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function isValidated(): bool
    {
        return $this->validated;
    }

    public function setValidated(bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function getBlueLeft(): ?int
    {
        return (Color::Undefined === $this->color) ? -1 : $this->blueLeft;
    }

    public function setBlueLeft(int $blueLeft): static
    {
        $this->blueLeft = $blueLeft;

        return $this;
    }

    public function getRedLeft(): ?int
    {
        return (Color::Undefined === $this->color) ? -1 : $this->redLeft;
    }

    public function setRedLeft(int $redLeft): static
    {
        $this->redLeft = $redLeft;

        return $this;
    }

    public function getWhiteLeft(): ?int
    {
        return (Color::Undefined === $this->color) ? -1 : $this->whiteLeft;
    }

    public function setWhiteLeft(int $whiteLeft): static
    {
        $this->whiteLeft = $whiteLeft;

        return $this;
    }

    public function getPrices(): ?Prices
    {
        return $this->prices;
    }

    public function setPrices(?Prices $prices): static
    {
        $this->prices = $prices;

        return $this;
    }
}
