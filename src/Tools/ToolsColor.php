<?php

namespace Tempo\Tools;

use Tempo\Enum\Color;

class ToolsColor
{
    protected const CHAR_TO_TEXT = [
        'french' => [
            'B' => 'bleu',
            'W' => 'blanc',
            'R' => 'rouge',
        ],
        'english' => [
            'B' => 'blue',
            'W' => 'white',
            'R' => 'red',
        ],
    ];

    public static function getEmojiFromValue(string $value): string
    {
        return match ($value) {
            'B' => '🔵',
            'W' => '⚪',
            'R' => '🔴',
            default => '🟠',
        };
    }

    public static function getEmojiCodeFromValue(string $value): string
    {
        return match ($value) {
            'B' => '&#x1F535;',
            'W' => '&#x26AA;',
            'R' => '&#x1F534;',
            default => '&#x1F7E0;',
        };
    }

    public static function getEmojiFromColor(Color $color): string
    {
        return self::getEmojiFromValue($color->value);
    }

    public static function getEnglishTextFromColor(Color $color): string
    {
        return self::getEnglishTextFromChar($color->value);
    }

    public static function getEnglishTextFromChar(string $char): string
    {
        return self::CHAR_TO_TEXT['english'][$char] ?? 'undefined';
    }

    public static function getFrenchTextFromColor(Color $getColor): string
    {
        return self::getFrenchTextFromChar($getColor->value);
    }

    public static function getFrenchTextFromChar(string $char): string
    {
        return self::CHAR_TO_TEXT['french'][$char] ?? 'inconnu';
    }
}
