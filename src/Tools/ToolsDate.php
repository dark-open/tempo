<?php

namespace Tempo\Tools;

class ToolsDate
{
    public const NUMBER_SUNDAY_ISO_8601 = '7';

    public static function getNow(bool $resetTime = true): \DateTime
    {
        $now = new \DateTime();

        if ($resetTime) {
            $now->setTime(0, 0);
        }
        $dateTimeZone = new \DateTimeZone('Europe/Paris');
        $now->setTimezone($dateTimeZone);

        return $now;
    }

    public static function getNextDay(?\DateTime $date = null, bool $resetTime = true): \DateTime
    {
        if (!$date) {
            $date = self::getNow($resetTime);
        }

        $nextDay = (clone $date)->add(new \DateInterval('P1D'));

        if ($resetTime) {
            $nextDay->setTime(0, 0);
        }

        return $nextDay;
    }

    public static function getPreviousDay(?\DateTime $date = null, bool $resetTime = true): \DateTime
    {
        return self::getXPastDay(1, clone $date, $resetTime);
    }

    public static function getXPastDay(int $dayInPast, ?\DateTime $date = null, bool $resetTime = true): \DateTime
    {
        if (!$date) {
            $date = self::getNow($resetTime);
        }

        $pastDay = (clone $date)->sub(new \DateInterval('P'.$dayInPast.'D'));

        if ($resetTime) {
            $pastDay->setTime(0, 0);
        }

        return $pastDay;
    }

    public static function getSmaller(\DateTime $x, \DateTime $y): \DateTime
    {
        return ($x < $y) ? $x : $y;
    }

    public static function isSunday(\DateTime $dateTime): bool
    {
        return self::NUMBER_SUNDAY_ISO_8601 === $dateTime->format('N');
    }

    public static function frenchDate(\DateTime $dateTime): string
    {
        $formatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);

        return $formatter->format($dateTime);
    }

    /**
     * @throws \Exception
     */
    public static function isAfterValidatedTime(): bool
    {
        $now = self::getNow(false);
        $reference = new \DateTime($now->format('Y-m-d').' 10:30:00');

        return $now >= $reference;
    }

    public static function isLeapYear(int $year): bool
    {
        return 0 === $year % 4 && (0 !== $year % 100 || 0 === $year % 400);
    }

    public static function isFirstOfSeptember(\DateTime $dateTime): bool
    {
        return '09' === $dateTime->format('m') && '01' === $dateTime->format('d');
    }

    public static function getPastFirstOfSeptember(\DateTime $date, bool $resetTime = true): \DateTime
    {
        $dateMonthInt = intval($date->format('m'));
        $dateYearInt = intval($date->format('Y'));
        if ($dateMonthInt > 9) {
            $return = \DateTime::createFromFormat('Ymd', $dateYearInt.'0901', new \DateTimeZone('Europe/Paris'));
        } else {
            $return = \DateTime::createFromFormat('Ymd', ($dateYearInt - 1).'0901', new \DateTimeZone('Europe/Paris'));
        }

        if ($resetTime) {
            $return = $return->setTime(0, 0);
        }

        return $return;
    }
}
