<?php

namespace Tempo\Enum;

enum Color: string
{
    case Blue = 'B';
    case White = 'W';
    case Red = 'R';
    case Undefined = 'U';

    /**
     * @return array<int,string>
     */
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function getColorFromString(string $strColor): Color
    {
        $string = ucfirst(strtolower($strColor));
        foreach (self::cases() as $status) {
            if ($string === $status->name) {
                return $status;
            }
        }

        return self::Undefined;
    }
}
